# Best Regards, Tawkir Alif
# Follow me on
- Facebook: https://www.facebook.com/tawkiralifa
- Instagram: https://www.instagram.com/tawkiralif
- Gitlab: https://gitlab.com/TawkirAlif
- Github: https://github.com/TawkirAlif

# Messenger Real Time Chat App

A beginners swift project to create a real time chat application in Swift 5 using Firebase.

## Features
- Facebook  Log In
- Google Sign In
- Email/Pass Registration / Log In
- Photo Messages
- Video Messages
- Real Time Conversations
- Location Messages
- Search for Users
- Deleting Conversations
- User Profile
- Dark Mode Support

Take a look at the free playlist on YouTube for a step by step guide how to build this project!
